applications_photos

## 1.获取最新代码，代码仓如下：

[applications_photos]: https://gitee.com/openharmony/applications_photos

## 2.编译构建

### 2.1 IDE配置Release编译包类型

如图在默认配置后面增加参数：-p debuggable= false

![](D:\f91448f4-0e0f-4885-aec5-25a19d5a6ff8.png)

### 2.2 签名

获取 **[signcenter_tool](https://gitee.com/openharmony/signcenter_tool)**仓中的签名工具

#### 2.2.1 自动签名

IDE按照如下配置签名配置，password :123456，其中Profile file栏配置为图库代码中的p7b文件路径：applications_photos\signature\photos.p7b。

![](D:\a83e1e81-54f5-4990-b9cd-b96be77fcb13.png)

配置完成后，Buid Haps(s)在applications_photos\entry\build\default\outputs\default路径下生成entry-default-signed.hap，可直接安装。

#### 2.2.2  手动签名

使用签名命令手动签名

将下列文件预置于下载的signcenter_tool仓目录下：

1.Buid Haps(s)在applications_photos\entry\build\default\outputs\default路径下生成的entry-default-unsigned.hap

2.hapsigntoolv2.jar

3.图库代码仓applications_photos\signature\下面的photos.p7b文件



在该目录下使用如下命令行或者在该目录下创建编辑如下命令的bat文件：

java -jar hapsigntoolv2.jar sign -mode localjks -privatekey "OpenHarmony Application Release" -inputFile entry-default-unsigned.hap -outputFile Photos.hap -signAlg SHA256withECDSA -keystore OpenHarmony.p12 -keystorepasswd 123456 -keyaliaspasswd 123456 -profile photos.p7b -certpath OpenHarmonyApplication.pem -profileSigned 1

签名成功后生成entry-default-signed.hap，可直接安装。



